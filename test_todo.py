import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains


def test_todo():
    # подключаем webdriver
    driver = WebDriver(executable_path='D://selenium//chromedriver.exe')

    # переходим на сайт todo
    driver.get('http://todomvc.com/examples/react/')

    # находим поле ввода
    xpath_add = '//input[@class="new-todo"]'
    target_add = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.XPATH, xpath_add)))

    # создаем task1
    target_add.send_keys("task1" + Keys.ENTER)

    # находим созданную задачу task1
    task1 = driver.find_element_by_xpath('//label[text()="task1"]')

    # двойной клик по задаче
    actionchains = ActionChains(driver)
    actionchains.double_click(task1).perform()

    # редактируем задачу
    edit_task1 = driver.find_element_by_xpath('//input[@class="edit"]')
    edit_task1.send_keys(" edited" + Keys.ENTER)

    # отметим задачу task1 edited как выполненную
    new_task1 = driver.find_element_by_xpath('//label[text()="task1 edited"]')
    toggle_task = new_task1.find_element_by_xpath('//input[@class="toggle"]')
    toggle_task.click()

    # перейдем на активные задачи
    driver.find_element_by_xpath('//a[@href="#/active"]').click()

    # проверка на пустой лист активных задач
    try:
        driver.find_element_by_xpath('//label[text()="task1 edited"]').is_displayed()
    except NoSuchElementException:
        print(None)

    # перейдем на выполненные задачи
    driver.find_element_by_xpath('//a[@href="#/completed"]').click()

    # сделаем задачу task1 edited активной снова
    driver.find_element_by_xpath('//input[@class="toggle"]').click()

    # создадим новую задачу task2 и task3
    driver.find_element_by_xpath('//a[@href="#/"]').click()
    target_add.send_keys("task2" + Keys.ENTER)
    target_add.send_keys("task3" + Keys.ENTER)

    # находим созданную задачу task2 и task3
    task2 = driver.find_element_by_xpath('//label[text()="task2"]')
    task3 = driver.find_element_by_xpath('//label[text()="task3"]')

    # отметим задачу task2 как выполненную
    toggle_task2 = task2.find_element_by_xpath('(//input[@class="toggle"])[2]')
    toggle_task2.click()

    # очистка всех выполненных задач
    driver.find_element_by_xpath('//button[@class="clear-completed"]').click()

    # проверка на отсутствие task2
    try:
        driver.find_element_by_xpath('//label[text()="task2"]').is_displayed()
    except NoSuchElementException:
        print(None)

    # отметить все задачи как выполненные
    toggle_all = driver.find_element_by_xpath('//label[@for="toggle-all"]')
    toggle_all.click()

    # сделать все задачи активными
    toggle_all.click()

    # проверка актирвности всех задач
    driver.find_element_by_xpath('//a[@href="#/active"]').click()
    new_task1
    task3

    # удалить задачу task3 нажатием на х
    task3.click()
    driver.find_element_by_xpath('(/html/body/section/div/section/ul/li/div/button)[2]').click()

    # проверка остуствия task3
    try:
        driver.find_element_by_xpath('//label[text()="task3"]').is_displayed()
    except NoSuchElementException:
        print(None)

    # проверка наличия task1 edited
    new_task1

    time.sleep(1)

